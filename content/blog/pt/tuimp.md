---
title: 'O Universo no meu bolso (The Universe In My Pocket - tuimp)'
date: '2021-08-04'
summary: 'Livrinhos de astronomia em diversas línguas.'
tags: 
- 
  id: 'astrofisica'
  tag: 'astrofísica'
- 
  id: 'divulgacao-cientifica'
  tag: 'divulgação científica'
indexImage: blog/2021-08-04/tuimp-logo.jpg
---

O Universo no meu bolso ([tuimp, do inglês The Universe In My Pocket](https://tuimp.org)) é um projeto que disponibiliza livrinhos gratuitos sobre astronomia e astrofísica para todos. Os livrinhos são escritos por astrofísicos profissionais ou estudantes de doutorado. Eles podem ser lidos em uma tela ou impressos em uma única folha de papel, [que ao ser dobrada vira um lindo e pequenino livrinho](https://www.youtube.com/watch?v=rLh2tb9YvLU). Tanto crianças, adolescentes e adultos terão sua curiosidade saciada!

![Capa do TUIMP nº 03, O reino das galáxias](/img/content/blog/2021-08-04/tuimp-galaxias.png "Capa do TUIMP nº 03, O reino das galáxias")
*Capa para TUIMP nº 03, O reino das galáxias*

Os livros estão disponíveis em vários idiomas (Afaan Oromoo, English,
Español, Français, isiZulu, Italiano, Polski, Português, Română,
Shqip, Ελληνικά, Русский, Հայերեն, العربية – e mais por vir!). O
O projeto é liderado por Grażyna Stasińska, do Observatório de Paris. Eu
traduzi alguns dos livros para o português.

Para baixá-los, vá em [tuimp.org](https://tuimp.org)!
