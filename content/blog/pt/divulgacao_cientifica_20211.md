---
title: 'Divulgação científica de estudantes de Física 1 (2021-1)'
date: '2021-10-13'
summary: 'Vídeos, carrosséis do instagram, etc. sobre mecânica clássica.'
tags: 
- 
  id: 'fisica'
  tag: 'física'
- 
  id: 'divulgacao-cientifica'
  tag: 'divulgação científica'
- 
  id: 'ensino'
  tag: 'ensino'
indexImage: blog/2021-10-13/logo-divulgacao.jpg
---

Esses foram alguns dos materiais produzidos pelas minhas turmas de Física 1 (FSC5101; cursos: matemática, oceanografia, engenharias) e Física Geral 1-A (FSC5107; física bacharelado) na UFSC.
Todos os grupos abaixo concordaram em deixar os trabalhos públicos.
A proposta era preparar, em grupos, uma atividade de divulgação científica em redes sociais. O tema deveria contemplar algum aspecto do conteúdo da disciplina, que é mecânica clássica.

Essa atividade foi concebida pela Profa. Dra. Marina Hirota, e implementada em um curso coletivo de Física 1 dos professores Marina Hirota, Natalia Vale Asari e Oswaldo Ritter.

[![Vídeo no YouTube (4 min): Navegantes da física](/img/content/blog/2021-10-13/FSC5101-20211-Grupo01.jpg "Vídeo do grupo 1: Navegantes da física")Vídeo no YouTube (4 min): Navegantes da física](https://www.youtube.com/watch?v=7zhu05jUdxs).  
**Grupo 1, FSC5101**: Blenda Barbosa, Gillian Fachinetto Oliveira, Hilario Tadeu da Fonseca Júnior, Natalia Becker, Pedro Gundlach Prates de Campos.  
Público-alvo: adolescentes de 14 a 18 anos, que já tiveram contato com assuntos de Física 1.

[![Vídeo no Youtube (15 min): Derivadas e suas propriedades](/img/content/blog/2021-10-13/FSC5101-20211-Grupo03.jpg "Vídeo do grupo 3: lebre e tartaruga")Vídeo no Youtube (15 min): Derivadas e suas propriedades](https://www.youtube.com/watch?v=hNDRxTHOqsc).  
**Grupo 3, FSC5101**: Renan Rabelo Goularti, Sofia Meneghel Silva.  
Público-alvo: pessoas que tenham conhecimento de funções matemáticas, tais como alunos do ensino médio, já que os conteúdos fora disso são explicados no próprio vídeo.

[![Vídeo no Youtube (10 min): MRU e MRUV](/img/content/blog/2021-10-13/FSC5101-20211-Grupo04.jpg "Vídeo do grupo 4: lata")Vídeo no Youtube (10 min): MRU e MRUV](https://www.youtube.com/watch?v=rXeJPDSPMdQ).  
(Detalhe: a fórmula s = s0 + vt só pode ser aplicada quando não há aceleração.)  
**Grupo 4, FSC5101**: Alícia Pacheco Dias, Breno Fernando Vieira Rosa, Riccardo de Andrade Barbosa.  
Público-alvo: estudantes do ensino médio ou de Física 1, para alcançar principalmente não consegue assimilar fórmulas e queira ver um exemplo prático de aplicação.

[![5 vídeos no TikTok (1 a 3 min): Momento linear, lançamento de projéteis, queda livre, relatividade](/img/content/blog/2021-10-13/FSC5101-20211-Grupo05.jpg "Vídeo do grupo 4: queda livre e lançamento oblíquo")5 vídeos no TikTok (1 a 3 min): Momento linear, lançamento de projéteis, queda livre, relatividade](https://vm.tiktok.com/ZMRUcBFwM/)  
**Grupo 5, FSC5101**: Adrielson Echamendi da Silva, Gabriel Bevilacqua Barros Dias, Jaqueline Ribeiro Dias, N'Horque Seidi.  
Público alvo: estudantes em geral; mais voltado para alunos do ensino médio.

[![Vídeo no YouTube (5 min): Leis de Newton](/img/content/blog/2021-10-13/FSC5107-20211-Grupo01.jpg
"Vídeo do grupo 1: Leis de Newton")Vídeo no YouTube (5 min): Leis de
Newton](https://www.youtube.com/watch?v=hPkhmWeV6TI).  
**Grupo 1, FSC5107**: Arthur da Silva Zimermann, Bruno Leandro Cortizo Burgos, Jonathan João Ramos, Nicollas Leonardo Martins Moura.  
Público alvo: estudantes do ensino médio.

[![4 carroséis no Instagram: Método científico](/img/content/blog/2021-10-13/FSC5107-20211-Grupo02.jpg
"Carroséis no Instagram: Método científico")4 carroséis no Instagram: Método científico](https://www.instagram.com/meto.docientifico/).  
**Grupo 2, FSC5107**: Amanda Leticia Fischer, Elisa de Melo Alvarenga, Gabriella C. Grützmann Tomazini, Jassiara Raísa Schmitt, João Lucas Soares.  
Público alvo: público em geral.

[![Carrossel no Instagram: Como funciona o método científico?](/img/content/blog/2021-10-13/FSC5107-20211-Grupo03.jpg
"Carrossel no Instagram: Como funciona o método científico?")Carrossel no Instagram: Como funciona o método científico?](https://www.instagram.com/osfisicoturistas/).  
**Grupo 3, FSC5107**: Laura Carolina da Costa, Lorenzo de Oliveira Heald, Maria Augusta S. Carvalho, Matheus Ferrari, Robson Ricardo da Silva.  
Público alvo: público em geral.

[![Postagem em blog + vídeo: Princípios da Mecânica Clássica](/img/content/blog/2021-10-13/FSC5107-20211-Grupo04.jpg
"Postagem em blog + vídeo: Princípios da Mecânica Clássica")Postagem em blog + vídeo: Princípios da Mecânica Clássica](https://principiosdamecanicaclassica.blogspot.com/2021/09/principios-da-mecanica-classica.html)  
**Grupo 4, FSC5107**: Eduardo Bleichvel Oneda, Gabriel Miranda Lima, Luiz Eduardo Borges Cardoso, Paulo Carvalho Neto.  
Público alvo: Estudantes do ensino médio, ensino superior e público em geral.

[![Vídeo no YouTube (10 min): As leis de Newton e sua abordagens históricas](/img/content/blog/2021-10-13/FSC5107-20211-Grupo05.jpg
"Vídeo do grupo 5: As leis de Newton e sua abordagens históricas")Vídeo no YouTube (10 min): As leis de Newton e sua abordagens históricas](https://www.youtube.com/watch?v=Mgwbs5Bb4bc).  
**Grupo 5, FSC5107**: Amadeus Dal Vesco, Hugo Silva, Leonardo Nascimento, Maria Eduarda Ramos Pedro, Sofia G dos Santos, Vinícius W. Carqueija.  
Público alvo: público em geral.

[![Vídeo no YouTube (5 min): Queda livre](/img/content/blog/2021-10-13/FSC5107-20211-Grupo06.jpg
"Vídeo do grupo 1: Leis de Newton")Vídeo no YouTube (5 min): Queda livre](https://www.youtube.com/watch?v=Y02XYjogn1c).  
**Grupo 6, FSC5107**: Joao Pedro B. de Aguiar, João Vitor Mews Bastos, Thabata John Barreto.  
Público alvo: público em geral.
