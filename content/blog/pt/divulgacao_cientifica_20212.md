---
title: 'Divulgação científica de estudantes de Física 1 (2021-2)'
date: '2022-03-24'
summary: 'Vídeos, carrosséis do instagram, etc. sobre mecânica clássica.'
tags: 
- 
  id: 'fisica'
  tag: 'física'
- 
  id: 'divulgacao-cientifica'
  tag: 'divulgação científica'
- 
  id: 'ensino'
  tag: 'ensino'
indexImage: blog/2022-03-24/logo-divulgacao.jpg
---

Esses foram alguns dos materiais produzidos pelas minhas turmas de Física 1 (FSC5101; cursos: química, matemática, oceanografia, engenharias) na UFSC.
Todos os grupos abaixo concordaram em deixar os trabalhos públicos.
A proposta era preparar, em grupos, uma atividade de divulgação científica em redes sociais. O tema deveria contemplar algum aspecto do conteúdo da disciplina, que é mecânica clássica.

Essa atividade foi concebida pela Profa. Dra. Marina Hirota, e implementada em um curso coletivo de Física 1 dos professores Marina Hirota, Natalia Vale Asari e Oswaldo Ritter.

[![Carroséis no Instagram: Física por trás](/img/content/blog/2022-03-24/FSC5101-20212-tarde-Grupo01.jpg
"Carroséis no Instagram: Física por trás")Carroséis no Instagram: Física por trás](https://www.instagram.com/fisicaportras/).  
**Grupo 1, FSC5101 (tarde)**: Nichollas Ryan, Roberta Parissi, Vitoria Correa, Ananda Tagliari.  
Público alvo: público em geral.

[![Carrosséis no Instagram: Por que a Física é tão importante para os atletas?](/img/content/blog/2022-03-24/FSC5101-20212-tarde-Grupo02.jpg
"Carroséis no Instagram: Por que a Física é tão importante para os atletas?")Carroséis no Instagram: Por que a Física é tão importante para os atletas?](http://minerva.ufsc.br/~natalia/site/blog/2022-03-24/Grupo2-tarde.pdf).  
**Grupo 2, FSC5101 (tarde)**: Luís Otávio de Oliveira Magalhães, Sergio Roberto Lucca Junior.  
Público alvo: público em geral.

[![Vídeo no YouTube (4 min):Experiência sobre Terceira Lei de Newton](/img/content/blog/2022-03-24/FSC5101-20212-tarde-Grupo03.jpg "Vídeo: Experiência sobre Terceira Lei de Newton")Vídeo no YouTube (4 min): Experiência sobre Terceira Lei de Newton](https://youtu.be/mqi3imHoJBo).  
**Grupo 1, FSC5101 (tarde)**: Alícia Pacheco Dias, Nicolas Henrique.  
Público alvo: público em geral.


[![Carrosséis no Instagram: A Física da sinuca](/img/content/blog/2022-03-24/FSC5101-20212-manha-Grupo02.jpg
"Carroséis no Instagram: A Física da sinuca")Carroséis no Instagram: A Física da sinuca](http://minerva.ufsc.br/~natalia/site/blog/2022-03-24/Grupo2-manha.pdf).  
**Grupo 2, FSC5101 (manhã)**: Julia Gutierrez, Esther Cristina Fernades Silva, Kelly Christine de Paula da Rosa.  
Público alvo: público em geral.

[![Carrosséis no Instagram: Por que a hidrelétrica de Balbina não deu certo?](/img/content/blog/2022-03-24/FSC5101-20212-manha-Grupo03.jpg
"Carroséis no Instagram: Por que a hidrelétrica de Balbina não deu certo?")Carroséis no Instagram: Por que a hidrelétrica de Balbina não deu certo?](http://minerva.ufsc.br/~natalia/site/blog/2022-03-24/Grupo3-manha.mp4).  
**Grupo 3, FSC5101 (manhã)**: Hernan Felipe Pieta Biavatti, Nícolas Marino de Farias, Igor Raul de Lara Santos, Francielly Santos Silva, Micheli Berti Figueredo.  
Público alvo: público em geral.

Legenda: A Usina Hidrelétrica de Balbina foi considerada como a pior do mundo, segundo diversos cientistas da área, correlacionando área alagada e geração de energia elétrica, sendo uma tragédia econômica, ecológica e social. Desde o início do projeto, estudiosos já alertavam quanto aos riscos, porém, não havendo espaço para contestar decisões sob a regência de Governos Militares, foram completamente ignorados e o desastre foi consumado. Não à toa especialistas da área pregam a desativação da Usina e demolição completa de sua barragem. Sendo este um importante exemplo de conscientização a governos e empresas considerarem sempre o olhar da ciência sobre suas intervenções.

