---
title: 'Science outreach content created by Physics 1 students (2021-1)'
date: '2021-10-13'
summary: 'Videos, instagram carousels, etc. on classical mechanics.'
tags: 
- 
  id: 'physics'
  tag: 'physics'
- 
  id: 'science-outreach'
  tag: 'science outreach'
- 
  id: 'teaching'
  tag: 'teaching'
indexImage: blog/2021-10-13/logo-divulgacao.jpg
---

Some of the content created by students in my of Physics 1 courses (FSC5101: maths, oceanography, and engineering undergrads; FSC5107: physics undergrads) at UFSC in the 2021-1 term.
All the groups below have agreed to make their content public.
We asked them prepare, in groups, science outreach content to be shared in social media. The theme should encompass classical mechanics.

This activity was conceived by Dr. Marina Hirota, and implemented in a collective course by professors Marina Hirota, Natalia Vale Asari and Oswaldo Ritter.

Note: all content is in Portuguese.

[![YouTube video (4 min): Physics sailors](/img/content/blog/2021-10-13/FSC5101-20211-Grupo01.jpg "Group 1 video: Physics sailors")YouTube video (4 min): Physics sailors](https://www.youtube.com/watch?v=7zhu05jUdxs).  
**Group 1, FSC5101**: Blenda Barbosa, Gillian Fachinetto Oliveira, Hilario Tadeu da Fonseca Júnior, Natalia Becker, Pedro Gundlach Prates de Campos.  
Target audience: 14 to 18 year-old teenagers who have already had contact with Physics 1 topics.

[![Youtube video (15 min): Derivatives and their properties](/img/content/blog/2021-10-13/FSC5101-20211-Grupo03.jpg "Group 3 video: hare and tortoise")Youtube video (15 min): Derivatives and their properties](https://www.youtube.com/watch?v=hNDRxTHOqsc).  
**Group 3, FSC5101**: Renan Rabelo Goularti, Sofia Meneghel Silva.  
Target audience: people who have some knowledge about mathematical functions, such as secondary school students; other concepts are explained in the video itself.

[![Youtube video (10 min): constant velocity and constant acceleration](/img/content/blog/2021-10-13/FSC5101-20211-Grupo04.jpg "Group 4 video: food tin")Youtube video (10 min): constant velocity and constant acceleration](https://www.youtube.com/watch?v=rXeJPDSPMdQ).  
(NB: the formula s = s0 + vt can only be applied when there is no acceleration).  
**Group 4, FSC5101**: Alícia Pacheco Dias, Breno Fernando Vieira Rosa, Riccardo de Andrade Barbosa.  
Target audience: secondary school students, or Physics 1 students, aiming to reach those who have trouble working with formulas and would benefit from a practical example.

[![5 videos on TikTok (1 to 3 min): Linear momentum, projectile motion, free fall, relativity](/img/content/blog/2021-10-13/FSC5101-20211-Grupo05.jpg "Group 4 video: free fall and projectile motion")5 videos on TikTok (1 to 3 min): Linear momentum, projectile motion, free fall, relativity](https://vm.tiktok.com/ZMRUcBFwM/)  
**Group 5, FSC5101**: Adrielson Echamendi da Silva, Gabriel Bevilacqua Barros Dias, Jaqueline Ribeiro Dias, N'Horque Seidi.  
Target audience: students in general, especially secondary school students.

[![YouTube video (5 min): Newton's Laws](/img/content/blog/2021-10-13/FSC5107-20211-Grupo01.jpg
"Group video 1: Newton's Laws")YouTube video (5 min): Newton's Laws
Newton](https://www.youtube.com/watch?v=hPkhmWeV6TI).  
**Group 1, FSC5107**: Arthur da Silva Zimermann, Bruno Leandro Cortizo Burgos, Jonathan João Ramos, Nicollas Leonardo Martins Moura.  
Target audience: secondary school students.

[![4 carousels on Instagram: The scientific method](/img/content/blog/2021-10-13/FSC5107-20211-Grupo02.jpg
"Carousels on Instagram: The scientific method")4 carousels on Instagram: The scientific method](https://www.instagram.com/meto.docientifico/).  
**Group 2, FSC5107**: Amanda Leticia Fischer, Elisa de Melo Alvarenga, Gabriella C. Grützmann Tomazini, Jassiara Raísa Schmitt, João Lucas Soares.  
Target audience: general public.

[![Carousel on Instagram: How does the scientific method work?](/img/content/blog/2021-10-13/FSC5107-20211-Grupo03.jpg
"Carousel on Instagram: How does the scientific method work?")Carousel on Instagram: How does the scientific method work?](https://www.instagram.com/osfisicoturistas/).  
**Group 3, FSC5107**: Laura Carolina da Costa, Lorenzo de Oliveira Heald, Maria Augusta S. Carvalho, Matheus Ferrari, Robson Ricardo da Silva.  
Target audience: general public.

[![Blog post + video: Principles of Classical Mechanics](/img/content/blog/2021-10-13/FSC5107-20211-Grupo04.jpg
"Blog post + video: Principles of Classical Mechanics")Blog post + video: Principles of Classical Mechanics](https://principiosdamecanicaclassica.blogspot.com/2021/09/principios-da-mecanica-classica.html)  
**Group 4, FSC5107**: Eduardo Bleichvel Oneda, Gabriel Miranda Lima, Luiz Eduardo Borges Cardoso, Paulo Carvalho Neto.  
Target audience: secondary school students, undergrad students and general public.

[![YouTube video (10 min): Newton's laws and historical approaches](/img/content/blog/2021-10-13/FSC5107-20211-Grupo05.jpg "Group 5 video: Newton's laws and  historical approaches")YouTube video (10 min): Newton's laws and historical approaches](https://www.youtube.com/watch?v=Mgwbs5Bb4bc).  
**Group 5, FSC5107**: Amadeus Dal Vesco, Hugo Silva, Leonardo Nascimento, Maria Eduarda Ramos Pedro, Sofia G dos Santos, Vinícius W. Carqueija.  
Target audience: general public.

[![YouTube video (5 min): Free fall](/img/content/blog/2021-10-13/FSC5107-20211-Grupo06.jpg "Group 1 Video: Newton's Laws")YouTube video (5 min): Free fall](https://www.youtube.com/watch?v=Y02XYjogn1c).  
**Group 6, FSC5107**: Joao Pedro B. de Aguiar, João Vitor Mews Bastos, Thabata John Barreto.  
Target audience: general public.
