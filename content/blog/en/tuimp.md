---
title: 'The Universe In My Pocket (tuimp)'
date: '2021-08-04'
summary: 'Booklets about Astronomy in several languages.'
tags: 
- 
  id: 'astrophysics'
  tag: 'astrophysics'
- 
  id: 'science-outreach'
  tag: 'science outreach'
indexImage: blog/2021-08-04/tuimp-logo.jpg
---

The Universe In My Pocket ([tuimp](https://tuimp.org)) is a project
that makes free booklets on astronomy and astrophysics available to
all. The booklets are written by professional astrophysicists or PhD
students. They can be read on a screen or printed on a single sheet of
paper,
[folded and made into a cute little book](https://www.youtube.com/watch?v=rLh2tb9YvLU). Children,
teenagers and adults alike will get their curiosity quenched!

![Cover for TUIMP nr. 03, The real of galaxies](/img/content/blog/2021-08-04/tuimp-galaxies.png "Cover for TUIMP nr. 03, The real of galaxies")
*Cover for TUIMP nr. 03, The realm of galaxies*

The books are available in several languages (Afaan Oromoo, English,
Español, Français, isiZulu, Italiano, Polski, Português, Română,
Shqip, Ελληνικά, Русский, Հայերեն, العربية – and counting!). The
project is led by Grażyna Stasińska, a member of Paris Observatory. I
have translated a few of the books to Portuguese.

To download them, head over to [tuimp.org](https://tuimp.org)!
