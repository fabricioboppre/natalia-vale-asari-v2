---
title: 'DIG'
summary: 'O gás difuso ionizado (DIG, do inglês Diffuse Ionized Gas)
pode ser muito importante, mas é geralmente ignorado. Seu impacto
sobre as calibrações de metalicidade pode ser enorme!'
position: 1
click: default
indexImage: projects/dig_index.png
---

![DIG](/img/content/projects/dig.png 'DIG')

Preocupados com o DIG? Nós também! Assim como as nuvens to tipo cirrus são
importantes para a previsão do tempo, o gás difuso 
também impacta nossa compreensão das galáxias. Métodos que usam linhas
fortes para inferir a metalicidade nebular são especialmente
afetados. Para saber mais, veja nossos artigos e anais de conferência
abaixo.

- [Lacerda et al. (2018)](http://adsabs.harvard.edu/abs/2018MNRAS.474.3727L)
Diffuse ionized gas in galaxies across the Hubble sequence at the
CALIFA resolution.
- [Vale Asari et al. (2019)](https://ui.adsabs.harvard.edu/abs/2019MNRAS.489.4721V): Diffuse ionized gas and its effects on nebular metallicity estimates of star-forming galaxies.
- [Vale Asari & Stasińska (2021)](https://ui.adsabs.harvard.edu/abs/2021IAUS..359..371V): The importance of the diffuse ionized gas for interpreting galaxy spectra.
- [Vale Asari (2021)](https://ui.adsabs.harvard.edu/abs/2021arXiv210800076V)
: The role of the diffuse ionized gas in metallicity calibrations.
