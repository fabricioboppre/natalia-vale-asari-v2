---
title: 'BOND'
summary: 'BOND é um método que infere a abundância de oxigênio e
nitrogênio em regiões HII gigantes usando linhas fortes e semi-fortes
e comparando-as com uma extensa grade de modelos de fotoionização.'
position: 1
click: http://bond.ufsc.br
indexImage: projects/bond_index.jpg
---

# Interested in measuring N/O and O/H in HII regions? Our method BOND is based on photoionization models that take into account the effect of secondary parameters to line emission, such as the hardness of the ionizing field and nebulae geometry. BOND gives us license to marginalize over these and other nuisance parameters. Get the paper, the code and the data tables at ![](https://bond.ufsc.br).
