---
title: 'BOND'
summary: 'BOND is a method to infer oxygen and nitrogen abundances in giant HII regions by comparison with a large grid of photoionization models using strong and semi-strong lines.'
position: 1
click: http://bond.ufsc.br
indexImage: projects/bond_index.jpg
---

# Interested in measuring N/O and O/H in HII regions? Our method BOND is based on photoionization models that take into account the effect of secondary parameters to line emission, such as the hardness of the ionizing field and nebulae geometry. BOND gives us license to marginalize over these and other nuisance parameters. Get the paper, the code and the data tables at ![](https://bond.ufsc.br).
