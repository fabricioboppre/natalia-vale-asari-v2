---
title: 'DIG'
summary: 'The Diffuse Ionized Gas (DIG) may be really important, but
is usually ignored. Its impact to metallicity callibrations may be huge!'
position: 1
click: default
indexImage: projects/dig_index.png
---

![DIG](/img/content/projects/dig.png 'DIG')

Worried about the DIG? So are we! Like cirrus clouds are important for
the weather forecast, the diffuse gas in a galaxy also impacts our
understanding of galaxies. Strong line methods used to infer gas-phase
metallicities are especially affected. If would like to know more,
have a look at our papers and conference proceedings below.

- [Lacerda et al. (2018)](http://adsabs.harvard.edu/abs/2018MNRAS.474.3727L)
Diffuse ionized gas in galaxies across the Hubble sequence at the
CALIFA resolution.
- [Vale Asari et al. (2019)](https://ui.adsabs.harvard.edu/abs/2019MNRAS.489.4721V): Diffuse ionized gas and its effects on nebular metallicity estimates of star-forming galaxies.
- [Vale Asari & Stasińska (2021)](https://ui.adsabs.harvard.edu/abs/2021IAUS..359..371V): The importance of the diffuse ionized gas for interpreting galaxy spectra.
- [Vale Asari (2021)](https://ui.adsabs.harvard.edu/abs/2021arXiv210800076V)
: The role of the diffuse ionized gas in metallicity calibrations.

