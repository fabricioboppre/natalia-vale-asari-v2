# (Em português)

## Introdução

Este é um pequeno [site estático](https://www.sanity.io/what-is-a-static-site) construído com o framework [Next.js](https://nextjs.org) e cujo conteúdo está armazenado em arquivos [Markdown](https://guides.github.com/features/mastering-markdown/).

Você pode visitá-lo clicando [aqui](https://www.nataliavaleasari.net/). O site utiliza o modo de pré-renderização [Static Generation](https://nextjs.org/docs/basic-features/pages#static-generation-recommended).

A seção *Blog* traz algumas funcionalidades tradicionais como paginação, RSS e tags, todas construídas com os métodos e ferramentas oferecidos pelo Next.js e pelo React. O site é também [multilíngue](https://nextjs.org/docs/advanced-features/i18n-routing) e possui um [sitemap](https://www.sitemaps.org).

Natalia Vale Asari é astrônoma e professora universitária de física.

## Configuração

Se você não possui nenhuma experiência com o Next.js, comece por [aqui](https://nextjs.org/docs/getting-started).

Após clonar o projeto (`git clone git@bitbucket.org:fabricioboppre/natalia-vale-asari-v2.git`), não esqueça de instalar suas dependências (`npm install`).

Para o deployment, recomendo a [Vercel](https://nextjs.org/docs/deployment).

## Tradução

Os comentários ao longo do código-fonte estão em inglês. Se algum brasileiro interessado em aprender algo com seu código-fonte encontrar alguma dificuldade, é só entrar em [contato](mailto:fabricio.boppre@gmail.com).

## Licenças

O código-fonte deste site está compartilhado sob a licença MIT e seu conteúdo sob a licença CC BY-NC-SA 4.0. Para mais informações, leia o arquivo [LICENSES](LICENSES.md).

# (In English)

## Introduction

This is a small [static website](https://www.sanity.io/what-is-a-static-site) built with the [Next.js](https://nextjs.org) framework and whose content is stored in [Markdown](https://guides.github.com/features/mastering-markdown/) files.

You can visit it by clicking [here](https://www.nataliavaleasari.net/). The site uses the pre-rendering mode [Static Generation](https://nextjs.org/docs/basic-features/pages#static-generation-recommended).

The *Blog* section offers some traditional functionality like pagination, RSS and tags, all built with the tools and methods offered by Next.js and React. The site is also [multilingual](https://nextjs.org/docs/advanced-features/i18n-routing) and has a [sitemap](https://www.sitemaps.org).

Natalia Vale Asari is an astronomer and professor of Physics.

## Configuration

If you have no experience with Next.js, start [here](https://nextjs.org/docs/getting-started).

After cloning the project (`git clone git@bitbucket.org:fabricioboppre/natalia-vale-asari-v2.git`), don't forget to install its dependencies (`npm install`).

For deployment, I suggest [Vercel](https://nextjs.org/docs/deployment).

## Licenses

The source code of this website is shared under the MIT license and the content is under the CC BY-NC-SA 4.0 license. For more information, see [LICENSES](LICENSES.md).