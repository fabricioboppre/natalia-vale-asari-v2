// See i18n locales definitions in /next.config.js

export const en = {
    header: {
        Title: "Dr Natalia Vale Asari",
        Local: "Departamento de Física, UFSC, Florianópolis, Brazil",
        Job: "Professora Adjunta C (equivalent to senior lecturer in the UK or assistant professor in the US) at Universidade Federal de Santa Catarina, Brazil",
        noJsMessage: "You need to enable JavaScript to browse in this site.",
    },
    navigationList: {
        CurrentProjects: "Current projects",
        Teaching: "Teaching",
        Publications: "Publications (ADS)",
        Thesis: "PhD thesis",
        CvUrl: "http://minerva.ufsc.br/~natalia/cv/ValeAsari_CV.pdf",
	CvName: "CV"
    },
    contact: {
        Title: "Contact",
        Country: "Brazil",
        Office: "Office",
    },
    projectsIndex: {
        Title: "Current projects",
        noProjects: "There is no project at this time.",
    },
    blogIndex: {
        Title: "Blog",
        noPosts: "There is no post at this time.",
        rssDescription: "Astrophysics, science and ramblings.",
    },
    pageNotFound: {
        Message: "This URL doens't have any content.",
    }
}

export const pt = {
    header: {
        Title: "Dra. Natalia Vale Asari",
        Local: "Departamento de Física, UFSC, Florianópolis, Brasil",
        Job: "Professora Adjunta C da Universidade Federal de Santa Catarina, Brasil",
        noJsMessage: "Você precisa habilitar JavaScript para navegar neste site.",
    },
    navigationList: {
        CurrentProjects: "Projetos atuais",
        Teaching: "Ensino",
        Publications: "Publicações (ADS)",
        Thesis: "Tese de doutorado",
        CvUrl: "http://lattes.cnpq.br/2206848030194988",
	CvName: "CV (Lattes)",
    },
    contact: {
        Title: "Contato",
        Country: "Brasil",
        Office: "Sala",
    },
    projectsIndex: {
        Title: "Projetos atuais",
        noProjects: "Não há nenhum projeto neste momento.",
    },
    blogIndex: {
        Title: "Blog",
        noPosts: "Não há nenhum post neste momento.",
        rssDescription: "Astrofísica, ciência e imprecisões sinápticas.",
    },
    pageNotFound: {
        Message: "Esta URL não possui nenhum conteúdo.",
    }
}
